<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

	</div><!-- #main -->

	<footer id="colophon" role="contentinfo">

			<?php
				/* A sidebar in the footer? Yep. You can can customize
				 * your footer with three columns of widgets.
				 */
				if ( ! is_404() )
					get_sidebar( 'footer' );
			?>

			<div id="site-generator">
				<?php do_action( 'twentyeleven_credits' ); ?>
				<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'twentyeleven' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'twentyeleven' ); ?>" rel="generator"><?php printf( __( 'Proudly powered by %s', 'twentyeleven' ), 'WordPress' ); ?></a>
			</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<div id="teller"> <h3 class="widget-title"><?php _e( 'Statistieken', 'twentyeleven' ); ?> </h3>  
  <!-- Start of StatCounter Code -->
     <script type="text/javascript">
var sc_project=4370575; 
var sc_invisible=0; 
var sc_partition=55; 
var sc_click_stat=1; 
var sc_text=4;
var sc_security="661c7a9f"; 
    </script>		

<?php _e( 'U bent bezoeker ', 'twentyeleven' ); ?>
   <script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script><noscript>
<div class="statcounter">
  <div align="center"><a title="myspace counter" href="http://www.statcounter.com/myspace/" target="_blank"></a></div>
  <p><a title="myspace counter" href="http://www.statcounter.com/myspace/" target="_blank"></a>
  <a title="myspace counter" href="http://www.statcounter.com/myspace/" target="_blank"><img src="http://c.statcounter.com/4370575/0/661c7a9f/0/" alt="myspace counter" class="statcounter" /></a></p>
</div></noscript>
<!-- End of StatCounter Code --></div></div>
</body>
</html>