<?php
/*
Template Name: Sponsors
*/

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">
                 <div id="sponsorpagina">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>
                 </div><!-- #spnsorpagina -->
			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_footer(); ?>