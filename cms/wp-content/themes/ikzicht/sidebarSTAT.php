<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

$options = twentyeleven_get_theme_options();
$current_layout = $options['theme_layout'];

if ( 'content' != $current_layout ) :
?>



<div id="secondary" class="widget-area" role="complementary">
			<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

				<aside id="archives" class="widget">
					<h3 class="widget-title"><?php _e( 'Archives', 'twentyeleven' ); ?></h3>
					<ul>
						<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
					</ul>
				</aside>

				<aside id="meta" class="widget">
					<h3 class="widget-title"><?php _e( 'Meta', 'twentyeleven' ); ?></h3>
					<ul>
						<?php wp_register(); ?>
						<li><?php wp_loginout(); ?></li>
						<?php wp_meta(); ?>
					</ul>
				</aside>

			<?php endif; // end sidebar widget area ?>

    <!-- Start of StatCounter Code -->
     <script type="text/javascript">
var sc_project=4370575; 
var sc_invisible=0; 
var sc_partition=55; 
var sc_click_stat=1; 
var sc_text=4;
var sc_security="661c7a9f"; 
    </script>		
<div id="teller"> <h3 class="widget-title"><?php _e( 'Statistieken', 'twentyeleven' ); ?> </h3>
<?php _e( 'U bent bezoeker ', 'twentyeleven' ); ?>
    <script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script><noscript>
<div class="statcounter">
  <div align="center"><a title="myspace counter" href="http://www.statcounter.com/myspace/" target="_blank"></a></div>
  <p><a title="myspace counter" href="http://www.statcounter.com/myspace/" target="_blank"></a>
  <a title="myspace counter" href="http://www.statcounter.com/myspace/" target="_blank"><img src="http://c.statcounter.com/4370575/0/661c7a9f/0/" alt="myspace counter" class="statcounter" /></a></p>
</div></noscript>
<!-- End of StatCounter Code --></div></div>

<?php endif; ?>