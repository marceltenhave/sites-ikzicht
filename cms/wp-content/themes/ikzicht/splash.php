<?php
/**
 * Template Name: Splash
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage twentyten
 * @since Twenty Ten 1.0
 */

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" rel="shortcut icon">
<title>Inzicht in jezelf</title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

</head>
<body>  
<div id="ContainerHomepage">
  <div>
    <img src="<?php bloginfo('template_directory'); ?>/images/Splash04.jpg" alt="Ikzicht" height="1200" with="2100" border="0" usemap="#Map">
    <map name="Map">
      <area shape="rect" coords="0,0,2100,1200" href="<?php echo site_url(); ?>/bladzijde-een" alt="Klik hier om de site te bezoeken">
    </map>
  </div>
</div>
<?php
   /* Always have wp_footer() just before the closing </body>
    * tag of your theme, or you will break many plugins, which
    * generally use this hook to reference JavaScript files.
    */

    wp_footer();
?>
</body>
</html>
